package cz.cuni.mff.bookreader.util;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cz.cuni.mff.bookreader.R;
import cz.cuni.mff.bookreader.db.DictionaryDbHelper;
import cz.cuni.mff.bookreader.enums.WordType;
import cz.cuni.mff.bookreader.model.WordDefinitionsDialogModel;
import cz.cuni.mff.bookreader.model.WordDefinition;

public class DictionaryUtils {

    private static final Comparator<WordDefinition> definitionsComparator = new Comparator<WordDefinition>() {
        @Override
        public int compare(WordDefinition d1, WordDefinition d2) {
            return Integer.valueOf(d1.getType().getValue()).compareTo(d2.getType().getValue());
        }
    };

    /**
     * Shows word definition for specified word.
     * @param title Word that we want to find definition for.
     */
    public static WordDefinitionsDialogModel getWordDefinition(Context context, String title) {
        WordDefinitionsDialogModel model = new WordDefinitionsDialogModel();
        model.setDialogTitle(title);
        title = title.trim();
        title = title.toLowerCase();
        List<WordDefinition> temporaryDefs = DictionaryDbHelper.findWordDefinitions(context, title);
        if (temporaryDefs.size() == 0) {
            String newTitle = "";
            if (title.endsWith("ies")) {
                newTitle = title.substring(0, title.length() - 3);
                newTitle += "y";
            } else if (title.endsWith("s")) {
                newTitle = title.substring(0, title.length() - 1);
            } else if (title.endsWith("ed")) {
                newTitle = title.substring(0, title.length() - 2);
            } else if (title.endsWith("ing")) {
                newTitle = title.substring(0, title.length() - 3);
                if (newTitle.charAt(newTitle.length() - 1) == newTitle.charAt(newTitle.length() - 2)) {
                    newTitle = title.substring(0, title.length() - 1);
                }
            }
            temporaryDefs = DictionaryDbHelper.findWordDefinitions(context, newTitle);
            if (!temporaryDefs.isEmpty()) {
                model.setDialogTitle(newTitle);
            }
        }

        model.setDefinitions(merge(temporaryDefs));
        return model;
    }

    /**
     * Merges WordDefinition with the same WordType to the one WordDefinition.
     * @param definitions WordDefinitions for merge.
     * @return Merged WordDefinitions.
     */
    private static List<WordDefinition> merge(List<WordDefinition> definitions) {
        Collections.sort(definitions, definitionsComparator);

        List<WordDefinition> newDefinitions = new ArrayList<>();

        WordDefinition first = null;
        StringBuilder sb = null;
        for (WordDefinition d : definitions) {
            if (first != null ) {
                if (first.getType() != d.getType()) {
                    String s = sb.toString();
                    s = s.replaceAll("\\|", ", "); //in database, words are delimited by "|", we change it to ", " so textview can properly show words
                    first.setDefinition(s);
                    newDefinitions.add(first);
                    first = d;
                    sb.setLength(0); //reusing string builder
                } else {
                    sb.append("\n\n");
                    sb.append(d.getDefinition());
                }
            } else {
                first = d;
                sb = new StringBuilder(d.getDefinition());
            }
        }

        if (first != null) {
            String s = sb.toString();
            s = s.replaceAll("\\|", ", ");
            first.setDefinition(s);
            newDefinitions.add(first);
        }

        return newDefinitions;
    }

}