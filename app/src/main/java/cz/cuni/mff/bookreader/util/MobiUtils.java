package cz.cuni.mff.bookreader.util;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class MobiUtils {

    private static final String TAG = MobiUtils.class.getName();
    private static final int PALM_DATABASE_LENGTH = 78;

    //TODO: finish
    public static void parse(File mobi) {
        try {
            InputStream is = new FileInputStream(mobi);
            byte[] palmDbFormat = new byte[PALM_DATABASE_LENGTH];
            int count = is.read(palmDbFormat);
            if (count == -1) {
                Log.e(TAG, "Cannot read PalmDatabaseFormat from file " + mobi.getPath());
                return;
            }
            PalmDatabaseFormat format = new PalmDatabaseFormat();
            format.name = new String(Arrays.copyOfRange(palmDbFormat, 0, 32));
            format.type = new String(Arrays.copyOfRange(palmDbFormat, 60, 68));
            ByteBuffer buffer = ByteBuffer.wrap(palmDbFormat, 76, 2);
            buffer.order(ByteOrder.BIG_ENDIAN);
            format.recordCount = buffer.getShort();

            Log.i(TAG, format.name + " " + format.type + " " + format.recordCount);

        } catch (Exception e) {
            Log.e(TAG, "Exception occurred during parsing of file " + mobi.getParent() + " , " + e.getMessage());
        }
    }

    private static class PalmDatabaseFormat {

        String name;
        String type;
        int recordCount;

    }
}

