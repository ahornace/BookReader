package cz.cuni.mff.bookreader.util;

import android.content.Context;
import android.graphics.Typeface;

public class FontUtils {

    private static Typeface born;
    private static Typeface calendasPlus;
    private static Typeface calendasPlusBold;
    private static Typeface calendasPlusItalic;

    public static Typeface getBorn(Context context) {
        if (born == null) {
            born = Typeface.createFromAsset(context.getAssets(), "fonts/Born.otf");
        }
        return born;
    }

    public static Typeface getCalendasPlus(Context context) {
        if (calendasPlus == null) {
            calendasPlus = Typeface.createFromAsset(context.getAssets(), "fonts/Calendas_Plus.otf");
        }
        return calendasPlus;
    }

    public static Typeface getCalendasPlusBold(Context context) {
        if (calendasPlusBold == null) {
            calendasPlusBold = Typeface.createFromAsset(context.getAssets(),
                    "fonts/Calendas_Plus_Bold.otf");
        }
        return calendasPlusBold;
    }

    public static Typeface getCalendasPlusItalic(Context context) {
        if (calendasPlusItalic == null) {
            calendasPlusItalic = Typeface.createFromAsset(context.getAssets(),
                    "fonts/Calendas_Plus_Italic.otf");
        }
        return calendasPlusItalic;
    }
}
