package cz.cuni.mff.bookreader.model;

/**
 * POJO that represents navigation item in the drawer layout.
 */
public class NavItemModel {

    private String title;

    private int drawable;

    public NavItemModel(String title, int drawable) {
        this.title = title;
        this.drawable = drawable;
    }

    public String getTitle() {
        return title;
    }

    public int getDrawable() {
        return drawable;
    }

}
