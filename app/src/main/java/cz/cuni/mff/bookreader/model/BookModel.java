package cz.cuni.mff.bookreader.model;

import java.io.Serializable;

/**
 * POJO that represents book data.
 */
public class BookModel implements Serializable {

    private long id;
    private String title;
    private String author;
    private String contentFileLocation;
    private String coverImageLocation;

    private Double page;
    private Integer spineItem;

    private String epubFilename;
    private long epubFileSize;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContentFileLocation() {
        return contentFileLocation;
    }

    public void setContentFileLocation(String contentFileLocation) {
        this.contentFileLocation = contentFileLocation;
    }

    public String getCoverImageLocation() {
        return coverImageLocation;
    }

    public void setCoverImageLocation(String coverImageLocation) {
        this.coverImageLocation = coverImageLocation;
    }

    public Double getPage() {
        return page;
    }

    public void setPage(Double page) {
        this.page = page;
    }

    public Integer getSpineItem() {
        return spineItem;
    }

    public void setSpineItem(Integer spineItem) {
        this.spineItem = spineItem;
    }

    public String getEpubFilename() {
        return epubFilename;
    }

    public void setEpubFilename(String epubFilename) {
        this.epubFilename = epubFilename;
    }

    public long getEpubFileSize() {
        return epubFileSize;
    }

    public void setEpubFileSize(long epubFileSize) {
        this.epubFileSize = epubFileSize;
    }
}
