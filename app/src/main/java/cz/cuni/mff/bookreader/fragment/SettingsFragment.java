package cz.cuni.mff.bookreader.fragment;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.util.Log;

import cz.cuni.mff.bookreader.R;

/**
 * Settings fragment.
 */
public class SettingsFragment extends PreferenceFragment {

    private static final String TAG = SettingsFragment.class.getName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceManager.setDefaultValues(getActivity(), R.xml.preferences, false);
        addPreferencesFromResource(R.xml.preferences);

        SensorManager sensorManager = (SensorManager) getActivity().getSystemService(Activity.SENSOR_SERVICE);
        Sensor lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if (lightSensor == null) { //light sensor is not available
            Log.i(TAG, "Light sensor is not available, disabling option to set changing of background.");
            SwitchPreference changeBgPreference = (SwitchPreference)
                    findPreference(getString(R.string.pref_change_bg_according_to_light));
            changeBgPreference.setChecked(false);
            changeBgPreference.setEnabled(false);
        }
    }

}
