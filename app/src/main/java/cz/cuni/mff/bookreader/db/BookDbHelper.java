package cz.cuni.mff.bookreader.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import cz.cuni.mff.bookreader.model.BookModel;

/**
 * Database helper of book database.
 */
public class BookDbHelper extends AbstractDbHelper {

    private static final String TAG = BookDbHelper.class.getName();

    public static final String DB_NAME = "books_db";
    private static final int DB_VERSION = 1;

    public static final String BOOKS_TABLE_NAME = "books";
    public static final String KEY_BOOKS_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_AUTHOR = "author";
    public static final String KEY_CONTENT_FILE_LOCATION = "content_file_location";
    public static final String KEY_COVER_IMAGE_LOCATION = "cover_image_location";
    public static final String KEY_SPINE_ITEM = "spine_item"; //html file where the page is
    public static final String KEY_PAGE = "page";
    public static final String KEY_EPUB_FILENAME = "epub_filename";
    public static final String KEY_EPUB_FILESIZE = "epub_filesize";
    public static final String KEY_LAST_OPENED = "last_opened";

    private static final String BOOK_SELECTION_BY_ID = KEY_BOOKS_ID + " = ?";

    public BookDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    private static BookDbHelper instance;

    public static synchronized BookDbHelper getInstance(Context c) {
        if (instance == null)
            instance = new BookDbHelper(c);
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //create books table
        db.execSQL("CREATE TABLE " + BOOKS_TABLE_NAME + " (" + KEY_BOOKS_ID + " INTEGER PRIMARY KEY, " +
                KEY_NAME + " TEXT, " + KEY_AUTHOR + " TEXT, " + KEY_CONTENT_FILE_LOCATION + " TEXT, " +
                KEY_COVER_IMAGE_LOCATION + " TEXT, " +
                KEY_SPINE_ITEM + " INTEGER, " + KEY_PAGE + " REAL, " + KEY_EPUB_FILENAME + " TEXT, " +
                KEY_EPUB_FILESIZE + " INTEGER, " + KEY_LAST_OPENED + " INTEGER);");

        //creating unique index on name and size
        db.execSQL("CREATE UNIQUE INDEX " + BOOKS_TABLE_NAME + "_uk_idx ON " + BOOKS_TABLE_NAME +
                " (" + KEY_NAME + ", " + KEY_EPUB_FILENAME + ", " + KEY_EPUB_FILESIZE + ");");

        //creating index for last opened = faster searching
        db.execSQL("CREATE INDEX " + BOOKS_TABLE_NAME + "_date_idx ON " + BOOKS_TABLE_NAME +
                " (" + KEY_LAST_OPENED + ");");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + BOOKS_TABLE_NAME + ";");
        onCreate(db);
    }

    public static BookModel loadBook(long id, Context context) {
        BookModel book = new BookModel();

        BookDbHelper dbHelper = getInstance(context);
        SQLiteDatabase db = dbHelper.openDatabase();

        final String[] args = {"" + id};

        Cursor c = db.query(BOOKS_TABLE_NAME, null, BOOK_SELECTION_BY_ID, args, null, null, null);
        int nameColumn = c.getColumnIndex(KEY_NAME);
        int authorColumn = c.getColumnIndex(KEY_AUTHOR);
        int contentFileColumn = c.getColumnIndex(KEY_CONTENT_FILE_LOCATION);
        int coverImgColumn = c.getColumnIndex(KEY_COVER_IMAGE_LOCATION);
        int spineItemColumn = c.getColumnIndex(KEY_SPINE_ITEM);
        int epubFileNameColumn = c.getColumnIndex(KEY_EPUB_FILENAME);
        int epubFileSizeColumn = c.getColumnIndex(KEY_EPUB_FILESIZE);
        int pageColumn = c.getColumnIndex(KEY_PAGE);
        if (c.moveToNext()) {
            if (!c.isNull(pageColumn))
                book.setPage(c.getDouble(pageColumn));
            if (!c.isNull(spineItemColumn))
                book.setSpineItem(c.getInt(spineItemColumn));
            book.setTitle(c.getString(nameColumn));
            book.setAuthor(c.getString(authorColumn));
            book.setContentFileLocation(c.getString(contentFileColumn));
            book.setCoverImageLocation(c.getString(coverImgColumn));
            book.setEpubFilename(c.getString(epubFileNameColumn));
            book.setEpubFileSize(c.getLong(epubFileSizeColumn));
        } else {
            Log.e(TAG, "No book with given id: " + book.getId() + " exists.");
            return null;
        }
        c.close();
        dbHelper.closeDatabase();

        return book;
    }

    public static void updatePage(long id, int currentSpineItem, double page, Context context) {
        final BookDbHelper dbHelper = getInstance(context);
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final ContentValues cv = new ContentValues();
        cv.put(KEY_SPINE_ITEM, currentSpineItem);
        cv.put(KEY_PAGE, page);
        cv.put(KEY_LAST_OPENED, System.currentTimeMillis());

        final String[] args = {"" + id};
        db.update(BOOKS_TABLE_NAME, cv, BOOK_SELECTION_BY_ID, args);
    }

    //loading only necessary attributes, should load everything?
    public static List<BookModel> loadBooksAsListItems(Context context) {
        BookDbHelper dbHelper = getInstance(context);
        SQLiteDatabase db = dbHelper.openDatabase();
        final String[] columns = {BookDbHelper.KEY_BOOKS_ID, BookDbHelper.KEY_NAME,
                BookDbHelper. KEY_AUTHOR, BookDbHelper.KEY_COVER_IMAGE_LOCATION};
        Cursor c = db.query(BookDbHelper.BOOKS_TABLE_NAME, columns, null, null, null, null, null);

        List<BookModel> books = new ArrayList<>();

        try {
            int idColumn = c.getColumnIndex(BookDbHelper.KEY_BOOKS_ID);
            int titleColumn = c.getColumnIndex(BookDbHelper.KEY_NAME);
            int authorColumn = c.getColumnIndex(BookDbHelper.KEY_AUTHOR);
            int coverImageColumn = c.getColumnIndex(BookDbHelper.KEY_COVER_IMAGE_LOCATION);

            while (c.moveToNext()) {
                BookModel book = new BookModel();
                book.setId(c.getLong(idColumn));
                book.setAuthor(c.getString(authorColumn));
                book.setTitle(c.getString(titleColumn));
                book.setCoverImageLocation(c.getString(coverImageColumn));
                books.add(book);
            }
        } finally {
            c.close();
            dbHelper.closeDatabase();
        }

        return books;
    }

    public static void insertBooks(Context context, List<BookModel> books) {
        BookDbHelper dbHelper = BookDbHelper.getInstance(context);
        SQLiteDatabase db = dbHelper.openDatabase();
        ContentValues cv = new ContentValues();
        try {
            db.beginTransactionNonExclusive();

            for (BookModel book : books) {
                cv.put(KEY_NAME, book.getTitle());
                cv.put(KEY_AUTHOR, book.getAuthor());
                cv.put(KEY_EPUB_FILENAME, book.getEpubFilename());
                cv.put(KEY_EPUB_FILESIZE, book.getEpubFileSize());
                cv.put(KEY_CONTENT_FILE_LOCATION, book.getContentFileLocation());
                cv.put(KEY_COVER_IMAGE_LOCATION, book.getCoverImageLocation());
                long id = db.insert(BOOKS_TABLE_NAME, null, cv);
                book.setId(id);
                cv.clear();
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        dbHelper.closeDatabase();
    }

    public static boolean existsInDb(Context context, String epubName, long epubLength) {
        BookDbHelper dbHelper = getInstance(context);
        SQLiteDatabase db = dbHelper.openDatabase();

        final String[] columns = {KEY_BOOKS_ID};
        final String whereClause = KEY_EPUB_FILENAME + "= ? AND " + KEY_EPUB_FILESIZE + " = ?";
        final String[] args = {epubName, "" + epubLength};

        Cursor c = db.query(BOOKS_TABLE_NAME, columns, whereClause, args, null, null, null);
        try {
            return c.moveToNext();
        } finally {
            c.close();
            dbHelper.closeDatabase();
        }
    }

    public static List<BookModel> loadRecentlyRead(Context context) {
        BookDbHelper dbHelper = getInstance(context);
        SQLiteDatabase db = dbHelper.openDatabase();

        String whereClause = BookDbHelper.KEY_LAST_OPENED + " IS NOT NULL";
        final String[] columns = {BookDbHelper.KEY_BOOKS_ID, BookDbHelper.KEY_NAME,
                BookDbHelper. KEY_AUTHOR, BookDbHelper.KEY_COVER_IMAGE_LOCATION};
        Cursor c = db.query(BookDbHelper.BOOKS_TABLE_NAME, columns, whereClause, null, null,
                null, BookDbHelper.KEY_LAST_OPENED + " DESC");

        int idColumn = c.getColumnIndex(BookDbHelper.KEY_BOOKS_ID);
        int titleColumn = c.getColumnIndex(BookDbHelper.KEY_NAME);
        int authorColumn = c.getColumnIndex(BookDbHelper.KEY_AUTHOR);
        int coverImageColumn = c.getColumnIndex(BookDbHelper.KEY_COVER_IMAGE_LOCATION);

        List<BookModel> books = new ArrayList<>();
        try {
            while (c.moveToNext()) {
                BookModel book = new BookModel();
                book.setId(c.getLong(idColumn));
                book.setAuthor(c.getString(authorColumn));
                book.setTitle(c.getString(titleColumn));
                book.setCoverImageLocation(c.getString(coverImageColumn));
                books.add(book);
            }
        } finally {
            c.close();
            dbHelper.closeDatabase();
        }

        return books;
    }


}
