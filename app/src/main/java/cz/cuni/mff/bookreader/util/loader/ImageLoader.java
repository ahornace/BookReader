package cz.cuni.mff.bookreader.util.loader;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import cz.cuni.mff.bookreader.R;

/**
 * Class that loads images.
 */
public class ImageLoader {

	private final MemoryCache memoryCache = new MemoryCache();

    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    private static final int KEEP_ALIVE_TIME = 1;
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
    private final ThreadPoolExecutor executor = new ThreadPoolExecutor(
            NUMBER_OF_CORES, NUMBER_OF_CORES, KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT,
            new LinkedBlockingQueue<Runnable>());

    /**
     * Displays image in the given imageView.
     * @param url Url of the image.
     * @param imageView ImageView where to set the image.
     * @param size Size of the image.
     */
	public void displayImage(Activity activity, String url, ImageView imageView, int size) {
		Bitmap bitmap = memoryCache.get(url);
        imageView.setTag(url);
		if (bitmap != null)
			imageView.setImageBitmap(bitmap);
		else if (url == null)
            imageView.setImageResource(R.drawable.book_cover_no_shdw);
        else {
			LoaderRunnable runnable = new LoaderRunnable(activity, imageView, url, size);
            executor.execute(runnable);
		}
	}

    /**
     * Loads images.
     */
	private class LoaderRunnable implements Runnable {
		private ImageView view;
		private String uri;
        private int size;
        private Activity activity;

		public LoaderRunnable(Activity activity, ImageView imageView, String uri, int size) {
			view = imageView;
			this.uri = uri;
            this.size = size;
            this.activity = activity;
		}

        @Override
        public void run() {
            BitmapFactory.Options o1 = new BitmapFactory.Options();
            o1.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(uri, o1);

            // finding correct scale value
            int width = o1.outWidth;
            int	height = o1.outHeight;
            int scale = 1;
            while (true) {
                if (width / 2 < size || height / 2 < size)
                    break;
                width /= 2;
                height /= 2;
                scale *= 2;
            }

            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            final Bitmap bitmap = BitmapFactory.decodeFile(uri, o2);

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (bitmap != null && view.getTag() != null && view.getTag().equals(uri)) {
                        view.setImageBitmap(bitmap);
                        memoryCache.put(uri, bitmap);
                    } else {
                        view.setImageResource(R.drawable.book_cover_no_shdw);
                    }
                }
            });

        }
    }

	public void clearCache() {
		memoryCache.clear();
	}

}