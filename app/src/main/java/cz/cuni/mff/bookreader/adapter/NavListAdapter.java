package cz.cuni.mff.bookreader.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cz.cuni.mff.bookreader.R;
import cz.cuni.mff.bookreader.model.NavItemModel;
import cz.cuni.mff.bookreader.util.FontUtils;

public class NavListAdapter extends BaseAdapter {

    private Context context;
    private List<NavItemModel> items;

    public NavListAdapter(Context context, List<NavItemModel> navItems) {
        this.context = context;
        items = navItems;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listview_nav_item, parent, false);
        } else {
            view = convertView;
        }

        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(items.get(position).getTitle());
        title.setTypeface(FontUtils.getCalendasPlusBold(context));

        ImageView img = (ImageView) view.findViewById(R.id.logo);
        img.setImageResource(items.get(position).getDrawable());

        return view;
    }
}
