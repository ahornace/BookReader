package cz.cuni.mff.bookreader.fragment;

import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cz.cuni.mff.bookreader.R;
import cz.cuni.mff.bookreader.activity.ReadingActivity;
import cz.cuni.mff.bookreader.adapter.BooksListAdapter;
import cz.cuni.mff.bookreader.db.BookDbHelper;
import cz.cuni.mff.bookreader.model.BookModel;
import cz.cuni.mff.bookreader.util.EpubUtils;
import cz.cuni.mff.bookreader.util.ZipUtils;

/**
 * Fragment that represents user's library. Shows user ebooks as a list.
 */
public class BookshelfFragment extends ListFragment {

    private static final String TAG = BookshelfFragment.class.getName();

    private final ArrayList<BookModel> books = new ArrayList<>();
    private SharedPreferences prefs;

    private BooksListAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        adapter = new BooksListAdapter(getActivity(), books);
        setListAdapter(adapter);

        prefs = getActivity().getPreferences(Context.MODE_PRIVATE);
        if (!prefs.getBoolean(getString(R.string.books_loaded), false))
            new BookFinderTask().execute();
        else {
            new BookLoaderTask().execute();
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_books_list, container, false);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(this.getActivity(), ReadingActivity.class);
        intent.putExtra(getString(R.string.book_id_passed), id);
        startActivity(intent);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_library_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.load_books:
                new BookFinderTask().execute();
                break;
            default:
                Log.e(TAG, "Clicked menu item that was not defined.");
        }
        return super.onOptionsItemSelected(item);
    }

    private void sortAndAddBooks(List<BookModel> books) {
        this.books.addAll(books);
        Collections.sort(this.books, new Comparator<BookModel>() {
            @Override
            public int compare(BookModel b1, BookModel b2) {
                return b1.getTitle().compareTo(b2.getTitle());
            }
        });
        adapter.notifyDataSetChanged();
    }

    /**
     * Finds books, processes them and them shows them in the library fragment as a list.
     */
    private class BookFinderTask extends AsyncTask<Void, Void, List<BookModel>> {

        private ProgressDialog dialog;

        private final String TAG = BookFinderTask.class.getName();

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(getActivity(), getString(R.string.loading), getString(R.string.please_wait), true);
            super.onPreExecute();
        }

        @Override
        protected List<BookModel> doInBackground(Void... params) {

            List<File> files = new ArrayList<>();
            Log.i(TAG, "Searching for files...");
            searchFiles(Environment.getExternalStorageDirectory(), files, new FileFilter() {
                @Override
                public boolean accept(File f) {
                    return f.isDirectory() || f.getName().endsWith(".epub"); //TODO: add mobi
                }
            });
            Log.i(TAG, "searching finished");
            List<BookModel> books = new ArrayList<>();
            for (File f : files) {
                File epubRoot = unzipEpub(f);
                if (epubRoot == null)
                    continue;
                List<BookModel> models = EpubUtils.parse(epubRoot, f.getName(), f.length());
                books.addAll(models);
            }
            return books;
        }

        @Override
        protected void onPostExecute(List<BookModel> books) {
            Log.i(TAG, "Entering book records to the database...");
            BookDbHelper.insertBooks(getActivity(), books);

            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(getString(R.string.books_loaded), true);
            editor.apply();

            sortAndAddBooks(books);

            if (dialog.isShowing())
                dialog.dismiss();

        }

        /**
         * Recursively finds files.
         * @param f Starting directory.
         * @param list List of found files.
         * @param filter Filter for files.
         */
        private void searchFiles(File f, List<File> list, FileFilter filter) {
            if (f == null)
                return;
            File[] files = f.listFiles(filter);

            for (File file : files) {
                if (file.isDirectory())
                    searchFiles(file, list, filter);
                else
                    list.add(file);
            }
        }

        /**
         * Unzips epub.
         * @param epub Epub file.
         * @return File where the file was extracted.
         */
        private File unzipEpub(File epub) {
            if (BookDbHelper.existsInDb(getActivity(), epub.getName(), epub.length())) {
                Log.i(TAG, "File " + epub.getPath() + " was already unzipped, skipping...");
                return null;
            }

            File file = ZipUtils.unzipFile(getActivity(), epub, epub.getName() + epub.length());

            if (file == null) {
                Log.e(TAG, "Could not unzip file " + epub.getPath());
                return null;
            }
            File mimetype = new File(file.getPath() + File.separator + "mimetype");
            if (!EpubUtils.isMimeTypeValid(mimetype))
                return null;

            return file;
        }
    }

    /**
     * Loads already processed books.
     */
    private class BookLoaderTask extends AsyncTask<Void, Void, List<BookModel>> {

        private final String TAG = BookLoaderTask.class.getName();

        @Override
        protected List<BookModel> doInBackground(Void... params) {
            Log.i(TAG, "Loading of already unzipped ebooks started.");
            return BookDbHelper.loadBooksAsListItems(getActivity());
        }

        @Override
        protected void onPostExecute(List<BookModel> bookModels) {
            BookshelfFragment.this.books.clear();
            sortAndAddBooks(bookModels);
            Log.i(TAG, "Loading of already unzipped ebooks finished.");
        }
    }
}
