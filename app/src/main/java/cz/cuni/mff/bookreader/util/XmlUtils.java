package cz.cuni.mff.bookreader.util;

import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.StringWriter;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class XmlUtils {

    private static final String TAG = XmlUtils.class.getName();

    /**
     * @param element element we want to get the string value from
     * @param tagName name of the tag that the value is stored in
     * @return returns string value with tagName in element
     */
    public static String getTextValue(Element element, String tagName) {
        String textVal = null;
        NodeList nl = element.getElementsByTagName(tagName);
        if (nl != null && nl.getLength() > 0) {
            Element el = (Element) nl.item(0);
            textVal = el.getFirstChild().getNodeValue();
        }
        return textVal;
    }

    /**
     * Returns first element with given name.
     * @param element Element tag where we want to get element from.
     * @param tagName TagName of the tag we want.
     * @return The first element with the specified tagName of the specified element.
     */
    public static Element getFirstElementWithName(Element element, String tagName) {
        NodeList nl = element.getElementsByTagName(tagName);
        if (nl != null && nl.getLength() > 0)
            return (Element) nl.item(0);
        return null;
    }

    /**
     * Converts XML node to String.
     * @param node Node to be converted.
     * @return String representation of node.
     */
    private static String nodeToString(Node node) {
        StringWriter sw = new StringWriter();
        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            t.transform(new DOMSource(node), new StreamResult(sw));
        } catch (TransformerException e) {
            Log.e(TAG, "Transformer exception " + e.getMessage());
            return null;
        }
        return sw.toString();
    }


    public static String getFirstTagToStringWithName(String name, Document document) {
        Node node = XmlUtils.getFirstElementWithName(document.getDocumentElement(), name);
        if (node == null)
            return null;
        return nodeToString(node);
    }

}
