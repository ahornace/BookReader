package cz.cuni.mff.bookreader.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;

import java.lang.reflect.Field;

/**
 * Number picker to be used in preferences.
 */
public class NumberPickerPreference extends DialogPreference {

    private NumberPicker picker;
    private Integer number = 16;

    public NumberPickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);
        setSummary("" + number);
    }

    @Override
    protected View onCreateDialogView() {
        picker = new NumberPicker(getContext());
        picker.setMinValue(8);
        picker.setMaxValue(64);
        picker.setValue(number);
        return picker;
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            // needed when user edits the text field and clicks OK
            picker.clearFocus();
            setValue(picker.getValue());
        }
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        setValue(restoreValue ? getPersistedInt(number) : (Integer) defaultValue);
    }

    public void setValue(int value) {
        setSummary("" + value);
        if (shouldPersist()) {
            persistInt(value);
        }

        if (value != number) {
            number = value;
            notifyChanged();
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getInt(index, 16);
    }

}
