package cz.cuni.mff.bookreader.util.loader;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import android.graphics.Bitmap;

public class MemoryCache {
	
	private Map<String, Bitmap> cache = new LinkedHashMap<>(10, 1.5f, true);
	private long size = 0;
	private long limit = 1000000;

	public MemoryCache() {
		setLimit(Runtime.getRuntime().maxMemory() / 4);
	}
	
	public void setLimit(long newLimit) {
		limit = newLimit;
	}
	
	public synchronized Bitmap get(String id) {
		if (!cache.containsKey(id)) {
			return null;
		} else {
			return cache.get(id);
		}
	}
	
	public synchronized void put(String id, Bitmap bitmap) {
        if(cache.containsKey(id)) {
            size = size - getSizeInBytes(cache.get(id));
        }
        cache.put(id, bitmap);
        size = size + getSizeInBytes(bitmap);
        checkSize();
	}
	
	private long getSizeInBytes(Bitmap bitmap) {
		if(bitmap == null) {
			return 0;
		} else {
			return bitmap.getRowBytes()*bitmap.getHeight();
		}
	}
	
	private void checkSize() {
		if(size > limit) {
			Iterator<Entry<String, Bitmap>> iter = cache.entrySet().iterator();
			for (Entry<String, Bitmap> entry; iter.hasNext(); ) {
                entry = iter.next();
				size = size - getSizeInBytes(entry.getValue());
				iter.remove();
				if(size <= limit) {
					break;
				}
			}
		}
	}
	
	public synchronized void clear() {
        cache.clear();
		size = 0;
	}
	
}