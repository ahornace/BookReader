package cz.cuni.mff.bookreader.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Helper class for opening and closing db connection in concurrent environment.
 */
public abstract class AbstractDbHelper extends SQLiteOpenHelper {

    private int counter = 0;
    private SQLiteDatabase db;

    public AbstractDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /**
     * Opens new or returns already created db connection.
     * @return Db connection.
     */
    public synchronized SQLiteDatabase openDatabase() {
        counter++;
        if(counter == 1) {
            // Opening new database
            db = getWritableDatabase();
        }
        return db;
    }

    /**
     * Closes last db connection.
     */
    public synchronized void closeDatabase() {
        counter--;
        if(counter == 0) {
            // Closing database
            db.close();
        }
    }
}
