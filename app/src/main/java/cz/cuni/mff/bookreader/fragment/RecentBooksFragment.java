package cz.cuni.mff.bookreader.fragment;

import android.app.ListFragment;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import cz.cuni.mff.bookreader.R;
import cz.cuni.mff.bookreader.activity.ReadingActivity;
import cz.cuni.mff.bookreader.adapter.BooksListAdapter;
import cz.cuni.mff.bookreader.db.BookDbHelper;
import cz.cuni.mff.bookreader.model.BookModel;

public class RecentBooksFragment extends ListFragment {

    private List<BookModel> books;

    private BooksListAdapter adapter;

    @Override
    public void onResume() {
        super.onResume();
        books = new ArrayList<>();
        adapter = new BooksListAdapter(getActivity(), books);
        setListAdapter(adapter);
        new RecentBooksLoaderTask().execute();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_books_list, container, false);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(this.getActivity(), ReadingActivity.class);
        intent.putExtra(getString(R.string.book_id_passed), id);
        startActivity(intent);
    }

    private class RecentBooksLoaderTask extends AsyncTask<Void, Void, List<BookModel>> {

        @Override
        protected List<BookModel> doInBackground(Void... params) {
            return BookDbHelper.loadRecentlyRead(getActivity());
        }

        @Override
        protected void onPostExecute(List<BookModel> bookModels) {
            RecentBooksFragment.this.books.clear();
            RecentBooksFragment.this.books.addAll(bookModels);
            adapter.notifyDataSetChanged();
        }
    }
}
