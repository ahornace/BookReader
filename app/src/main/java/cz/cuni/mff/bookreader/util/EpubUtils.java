package cz.cuni.mff.bookreader.util;

import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import cz.cuni.mff.bookreader.model.BookModel;

public class EpubUtils {

    private static final String TAG = EpubUtils.class.getName();

    private static final String EPUB_MIMETYPE = "application/epub+zip";
    private static final String EPUB_MEDIA_TYPE = "application/oebps-package+xml";


    /**
     * Loads book information from unziped epub.
     * @param epubRoot File of the unzipped epub.
     * @param fileName Name of the epub file.
     * @param fileLength Length of the epub file.
     * @return Returns list of books that were in this epub file.
     */

    public static List<BookModel> parse(File epubRoot, String fileName, long fileLength) {
        Log.i(TAG, "Parsing file " + epubRoot.getPath());
        List<BookModel> books = new ArrayList<>();
        File container = new File(epubRoot.getPath() + File.separator + "META-INF" +
                File.separator + "container.xml");

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            Document dom = documentBuilder.parse(container);

            NodeList rootFiles = dom.getElementsByTagName("rootfile");
            if (rootFiles == null || rootFiles.getLength() == 0)
                return books;

            for (int j = 0; j < rootFiles.getLength(); j++) {
                BookModel book = new BookModel();
                Element rootFile = (Element) rootFiles.item(j);
                String mediaType = rootFile.getAttribute("media-type");
                String path = rootFile.getAttribute("full-path");

                if (!mediaType.equals(EPUB_MEDIA_TYPE)) {
                    Log.d(TAG, "Root file is not an epub: " + path);
                    continue;
                }
                File contentFile = new File(epubRoot + File.separator + path);

                Document content = documentBuilder.parse(contentFile);
                Element metadata = XmlUtils.getFirstElementWithName(content.getDocumentElement(), "metadata");

                Element manifest = XmlUtils.getFirstElementWithName(content.getDocumentElement(), "manifest");

                if (manifest == null) {
                    Log.e(TAG, "No manifest for the root file: " + path);
                    continue;
                }

                NodeList items = manifest.getElementsByTagName("item");
                if (items != null && items.getLength() > 0) {
                    for (int i = 0; i < items.getLength(); i++) {
                        Element item = (Element) items.item(i);
                        if (item.getAttribute("id").equals("cover") &&
                                item.getAttribute("media-type").equals("image/jpeg")) {
                            File img = new File(contentFile.getParent(), item.getAttribute("href"));
                            book.setCoverImageLocation(img.getPath());
                            break;
                        }
                    }
                }

                if (metadata != null) {
                    String title = XmlUtils.getTextValue(metadata, "dc:title");
                    String creator = XmlUtils.getTextValue(metadata, "dc:creator");
                    if (creator == null) {
                        creator = "Unknown author.";
                    }
                    book.setContentFileLocation(contentFile.getPath());
                    book.setTitle(title);
                    book.setAuthor(creator);
                } else {
                    Log.e(TAG, "No metadata for the book " + contentFile.getPath());
                    continue;
                }

                book.setEpubFilename(fileName);
                book.setEpubFileSize(fileLength);
                books.add(book);
            }

        } catch (Exception e) {
            Log.e(TAG, "Exception during processing container file: " + e.getMessage());
        }
        return books;
    }


    /**
     * Checks the mimetype.
     * @param mimeType Mimetype to check.
     * @return Result if the mimetype is valid.
     */
    public static boolean isMimeTypeValid(File mimeType) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(mimeType));
            String line;
            line = br.readLine();

            if (!line.equals(EPUB_MIMETYPE)) {
                Log.w(TAG, "Incorrect mimetype of file " + mimeType.getParent());
                return false;
            }
        } catch (IOException e) {
            Log.w(TAG, "Exception thrown when trying to load mimetype, " + e.getMessage());
            return false;
        }
        return true;
    }


}
