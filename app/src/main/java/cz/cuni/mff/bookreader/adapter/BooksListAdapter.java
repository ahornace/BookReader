package cz.cuni.mff.bookreader.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cz.cuni.mff.bookreader.R;
import cz.cuni.mff.bookreader.model.BookModel;
import cz.cuni.mff.bookreader.util.FontUtils;
import cz.cuni.mff.bookreader.util.loader.ImageLoader;

public class BooksListAdapter extends BaseAdapter {

    private Context context;
    private List<BookModel> books;
    private ImageLoader imageLoader;

    private float scaling;

    private static final int THUMBNAIL_SIZE = 112;

    public BooksListAdapter(Context context, List<BookModel> books) {
        this.context = context;
        this.books = books;
        imageLoader = new ImageLoader();

        scaling = context.getResources().getDisplayMetrics().density;
    }

    @Override
    public int getCount() {
        return books.size();
    }

    @Override
    public Object getItem(int position) {
        return books.get(position);
    }

    @Override
    public long getItemId(int position) {
        return books.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listview_book_item, parent, false);
        } else {
            view = convertView;
        }

        TextView title = (TextView) view.findViewById(R.id.bookTitle);
        TextView author = (TextView) view.findViewById(R.id.author);

        BookModel book = books.get(position);

        ImageView imageView = (ImageView) view.findViewById(R.id.bookCover);
        imageLoader.displayImage((Activity) context, book.getCoverImageLocation(), imageView, (int) (THUMBNAIL_SIZE * scaling));

        title.setText(book.getTitle());
        title.setTypeface(FontUtils.getCalendasPlus(context));
        author.setText(book.getAuthor());
        author.setTypeface(FontUtils.getCalendasPlus(context));

        return view;
    }
}
