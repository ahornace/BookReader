package cz.cuni.mff.bookreader.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cz.cuni.mff.bookreader.R;
import cz.cuni.mff.bookreader.model.ContentItemModel;
import cz.cuni.mff.bookreader.util.FontUtils;

public class ContentAdapter extends BaseAdapter {

    private Context context;
    private List<ContentItemModel> contentItems;
    //private Typeface calendasPlus;
    //private Typeface calendasPlusBold;

    public ContentAdapter(Context context, List<ContentItemModel> items) {
        contentItems = items;
        this.context = context;
        //calendasPlus = Typeface.createFromAsset(context.getAssets(), "fonts/Calendas_Plus.otf");
        //calendasPlusBold = Typeface.createFromAsset(context.getAssets(), "fonts/Calendas_Plus_Bold.otf");
    }

    public List<ContentItemModel> getContentItems() {
        return contentItems;
    }

    @Override
    public int getCount() {
        return contentItems.size();
    }

    @Override
    public Object getItem(int position) {
        return contentItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listview_content_item, parent, false);
        } else {
            view = convertView;
        }

        TextView title = (TextView) view.findViewById(R.id.contentTitle);
        title.setText(contentItems.get(position).getId());

        if (contentItems.get(position).isActive())
            title.setTypeface(FontUtils.getCalendasPlusBold(context));
        else
            title.setTypeface(FontUtils.getCalendasPlus(context));

        return view;
    }
}
