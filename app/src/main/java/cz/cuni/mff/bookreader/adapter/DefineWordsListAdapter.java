package cz.cuni.mff.bookreader.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cz.cuni.mff.bookreader.R;
import cz.cuni.mff.bookreader.model.WordDefinition;
import cz.cuni.mff.bookreader.util.FontUtils;

public class DefineWordsListAdapter extends BaseAdapter {

    private List<WordDefinition> wordDefinitions;
    private Context context;

    public DefineWordsListAdapter(Context context, List<WordDefinition> wordDefinitions) {
        this.context = context;
        this.wordDefinitions = wordDefinitions;
    }

    @Override
    public int getCount() {
        return wordDefinitions.size();
    }

    @Override
    public Object getItem(int position) {
        return wordDefinitions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listview_definition_item, parent, false);
        } else {
            view = convertView;
        }

        TextView type = (TextView) view.findViewById(R.id.wordType);
        WordDefinition def = wordDefinitions.get(position);
        type.setText(def.getType().toString());
        type.setTypeface(FontUtils.getCalendasPlusItalic(context));

        TextView content = (TextView) view.findViewById(R.id.content);
        content.setText(def.getDefinition());
        
        content.setTypeface(FontUtils.getCalendasPlus(context));

        return view;
    }
}