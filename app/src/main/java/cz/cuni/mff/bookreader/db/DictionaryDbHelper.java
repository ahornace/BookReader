package cz.cuni.mff.bookreader.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.cuni.mff.bookreader.R;
import cz.cuni.mff.bookreader.enums.WordType;
import cz.cuni.mff.bookreader.model.WordDefinition;

/**
 * Helper class for dictionary db.
 * Two tables are needed because one word can have multiple definitions with different word type,
 * for instance: love can be verb and also noun
 */
public class DictionaryDbHelper extends AbstractDbHelper {

    public static final String DB_NAME = "dictionary_db";
    private static final int DB_VERSION = 1;

    public static final String WORDS_TABLE_NAME = "words";
    public static final String KEY_WORD_ID = "id";
    public static final String KEY_WORD = "key_word";

    public static final String DEFINITIONS_TABLE_NAME = "definitions";
    public static final String KEY_DEFINITION_ID = "id";
    public static final String KEY_DEFINITION_PARENT_ID = "word_id";
    public static final String KEY_DEFINITION_TYPE = "type";
    public static final String KEY_DEFINITION = "definition";

    private static final String DICTIONARY_LOCATION = "dictionary/data.sql";

    private static DictionaryDbHelper instance;

    private static final String TAG = DictionaryDbHelper.class.getName();

    public DictionaryDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static synchronized DictionaryDbHelper getInstance(Context c) {
        if (instance == null)
            instance = new DictionaryDbHelper(c);
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //create words table
        db.execSQL("CREATE TABLE " + WORDS_TABLE_NAME + " (" + KEY_WORD_ID + " INTEGER PRIMARY KEY, " +
                KEY_WORD + " TEXT);");
        //create index on word = faster searching
        db.execSQL("CREATE INDEX " + WORDS_TABLE_NAME + "_" + KEY_WORD + "_idx ON " +
                WORDS_TABLE_NAME + " (" + KEY_WORD + ");");

        //create definitions table
        db.execSQL("CREATE TABLE " + DEFINITIONS_TABLE_NAME + " (" + KEY_DEFINITION_ID +
                " INTEGER PRIMARY KEY, " + KEY_DEFINITION_PARENT_ID + " INTEGER, " +
                KEY_DEFINITION_TYPE + " INTEGER, " + KEY_DEFINITION + " TEXT, FOREIGN KEY(" +
                KEY_DEFINITION_PARENT_ID + ") REFERENCES " + WORDS_TABLE_NAME + "(" + KEY_WORD_ID + "));");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + WORDS_TABLE_NAME + ";");
        db.execSQL("DROP TABLE IF EXISTS " + DEFINITIONS_TABLE_NAME + ";");

    }

    public static boolean initDb(Context context) {
        DictionaryDbHelper dbHelper = DictionaryDbHelper.getInstance(context);
        SQLiteDatabase db = dbHelper.openDatabase();

        context.deleteDatabase(BookDbHelper.DB_NAME);
        //db.delete(DictionaryDbHelper.WORDS_TABLE_NAME, null, null);
        //db.delete(DictionaryDbHelper.DEFINITIONS_TABLE_NAME, null, null);
        //boolean result = true;

        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open(DICTIONARY_LOCATION)));
            String line;
            int i = 0;

            while ((line = reader.readLine()) != null) {
                if (i % 1000 == 0) {
                    Log.d(TAG, "Dictionary rows inserted: " + i);
                    if (i != 0) {
                        db.setTransactionSuccessful();
                        db.endTransaction();
                    }
                    db.beginTransactionNonExclusive();
                }
                db.execSQL(line);
                i++;
            }
            db.setTransactionSuccessful();
            reader.close();
        } catch (Exception e) {
            Log.e(TAG, "Exception during initialization of dictionary " + e.getMessage());
            return false;
        } finally {
            db.endTransaction();
        }
        dbHelper.closeDatabase();

        return true;
    }

    /**
     * Finds WordDefinitions in database.
     * @param title Word that we are looking for.
     * @return WordDefinition for specified word.
     */
    public static List<WordDefinition> findWordDefinitions(Context context, String title) {
        DictionaryDbHelper dbHelper = getInstance(context);
        SQLiteDatabase db = dbHelper.openDatabase();

        String[] wordArgs = {title};
        String[] wordIdColumn = {KEY_WORD_ID};
        String wordWhereColumn = KEY_WORD + " = ?";
        Cursor c = db.query(WORDS_TABLE_NAME, wordIdColumn, wordWhereColumn, wordArgs, null, null, null);

        int index = c.getColumnIndex(KEY_WORD_ID);
        int id = -1;
        if (c.moveToNext()) {
            id = c.getInt(index);
        }
        c.close();

        if (id == -1) {
            dbHelper.closeDatabase();
            return new ArrayList<>(0);
        }

        final String[] definitionColumns = {KEY_DEFINITION_TYPE, KEY_DEFINITION};
        final String definitionWhereClause = KEY_DEFINITION_PARENT_ID + " = ?";
        final String[] definitionArgs = {"" + id};
        c = db.query(DEFINITIONS_TABLE_NAME, definitionColumns, definitionWhereClause, definitionArgs, null, null, null);

        int definition = c.getColumnIndex(KEY_DEFINITION);
        int type = c.getColumnIndex(KEY_DEFINITION_TYPE);
        List<WordDefinition> definitions = new ArrayList<>();

        try {
            while (c.moveToNext()) {
                WordDefinition wd = new WordDefinition();
                wd.setDefinition(c.getString(definition));
                wd.setType(WordType.getType(c.getInt(type)));
                definitions.add(wd);
            }
        } finally {
            c.close();
            dbHelper.closeDatabase();
        }

        return definitions;
    }

}
