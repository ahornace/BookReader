package cz.cuni.mff.bookreader.util;

import android.content.Context;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Utility that unzips files.
 */
public class ZipUtils {

    private static final String TAG = ZipUtils.class.getName();

    /**
     * Unzips file.
     * @param context Context for getting internal directory.
     * @param zippedFile Zipped files.
     * @param newFileName FileName of the file where to unzip the zipped file.
     * @return File where the file was unzipped.
     */
    public static File unzipFile(Context context, File zippedFile, String newFileName) {
        File internalDir = context.getFilesDir();
        Log.i(TAG, "Unzipping file " + zippedFile.getPath());

        File newFile = new File(internalDir.getPath() + File.separator + newFileName); //unpack to the internal directory
        if (newFile.exists())
            return newFile;

        ZipInputStream zis = null;
        FileOutputStream fos = null;
        try {
            InputStream is = new FileInputStream(zippedFile);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;

            byte[] buffer = new byte[1024];
            int count;

            while ((ze = zis.getNextEntry()) != null) {
                String name = ze.getName();

                File file = new File(newFile.getPath() + File.separator + name);
                if (ze.isDirectory()) {
                    if (!file.exists()) {
                        if (!file.mkdirs()) {
                            Log.e(TAG, "Could not create dirs for directory " + file.getPath());
                        }
                    }
                    continue;
                }

                if (!file.getParentFile().exists()) {
                    if (!file.getParentFile().mkdirs()) {
                        Log.e(TAG, "Could not create dirs for directory " + file.getParentFile().getPath());
                    }
                }

                if (!file.createNewFile()) {
                    Log.e(TAG, "Could not create a new file " + file.getPath());
                }

                try {
                    fos = new FileOutputStream(file.getPath());
                    while ((count = zis.read(buffer)) != -1) {
                        fos.write(buffer, 0, count);
                    }
                } finally {
                    if (fos != null)
                        fos.close();
                }
                zis.closeEntry();
            }
        } catch (IOException e) {
            Log.e(TAG, "Exception occurred during unzipping of file " + newFile.getPath() + " : " + e.getMessage());
            return null;
        } finally {
            try {
                if (zis != null)
                    zis.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close stream " + e.getMessage());
            }
        }

        return newFile;
    }

}
