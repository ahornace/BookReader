package cz.cuni.mff.bookreader.model;

import cz.cuni.mff.bookreader.enums.WordType;

/**
 * POJO that represents word definition.
 */
public class WordDefinition {

    private WordType type;

    private String definition;

    public WordType getType() {
        return type;
    }

    public void setType(WordType type) {
        this.type = type;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }
}
