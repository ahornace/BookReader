package cz.cuni.mff.bookreader.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import cz.cuni.mff.bookreader.R;
import cz.cuni.mff.bookreader.activity.ReadingActivity;

public class EbookWebView extends WebView {

    private static final String TAG = EbookWebView.class.getName();
    private static final float SCROLL_THRESHOLD = 0.2f;

    private float xClicked;
    private float yClicked;
    private boolean isOnClick;
    private long onDown;

    private boolean popDialogShown;

    private boolean defineActionModeShown = false;
    private boolean showContentActionModeShown = false;

    private ActionMode actionMode;

    private ReadingActivity activity;

    private int currentPage = 1;

    private int maxPage;

    private boolean loadMaxPage = false; //needed for determining which javascript function to call after page finished loading

    private boolean loading = true;

    public EbookWebView(Context context) {
        super(context);
        this.activity = (ReadingActivity) context;
        init();
    }

    public EbookWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.activity = (ReadingActivity) context;
        init();
    }

    public EbookWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.activity = (ReadingActivity) context;
        init();
    }

    private void init() {
        addJavascriptInterface(this, "jsInterface");

        setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                loading = true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                loading = false;
                if (loadMaxPage)
                    loadUrl("javascript:jsInterface.onLoadAndReturnMaxPage(getMaxPage())");
                else
                    loadUrl("javascript:jsInterface.onGetCurrentAndMaxPage(getCurrentAndMaxPage())");
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("http") || url.startsWith("mailto:")) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    activity.startActivity(i);
                    return true;
                } else if (url.endsWith(ReadingActivity.BOOK_CONTAINER_NAME)) {
                    return false;
                } else if (url.startsWith("file://")) {
                    url = url.substring(7);
                    activity.loadFileToWebView(url);
                    return true;
                } else {
                    return true;
                }
            }
        });

        setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(@NonNull ConsoleMessage consoleMessage) {
                Log.d(TAG, consoleMessage.message() + " -- From line " + consoleMessage.lineNumber() +
                        " of " + consoleMessage.sourceId());
                return super.onConsoleMessage(consoleMessage);
            }
        });
    }

    public void setLoadMaxPage(boolean loadMaxPage) {
        this.loadMaxPage = loadMaxPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getMaxPage() {
        return maxPage;
    }

    public void setPopDialogShown(boolean popDialogShown) {
        this.popDialogShown = popDialogShown;
    }

    @Override
    public ActionMode startActionMode(ActionMode.Callback callback) {
        if (callback instanceof ShowContentsActionModeCallback) {
            return super.startActionMode(callback);
        }
        DefineWordActionModeCallback callback1 = new DefineWordActionModeCallback();
        return super.startActionMode(callback1);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (showContentActionModeShown) {
            clearFocus();
            showContentActionModeShown = false;
            actionMode.finish();
            return super.onTouchEvent(event);
        }
        if (defineActionModeShown || loading)
            return super.onTouchEvent(event);

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                xClicked = event.getX() / getWidth();
                yClicked = event.getY() / getHeight();
                isOnClick = true;
                onDown = System.currentTimeMillis();
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                if (isOnClick) {
                    if (popDialogShown) {
                        popDialogShown = false;
                        activity.popDialog();
                        break;
                    }
                    long now = System.currentTimeMillis();
                    if (now - onDown < 500) {
                        if (xClicked < 0.4)
                            previous();
                        else if (xClicked > 0.6)
                            next();
                        else {
                            startActionMode(new ShowContentsActionModeCallback());
                        }
                    }
                } else {
                    float xUp = event.getX() / getWidth();
                    if (xUp - xClicked > 0.2)
                        previous();
                    else if (xUp - xClicked < -0.2)
                        next();

                    if (popDialogShown) {
                        popDialogShown = false;
                        activity.popDialog();
                    }
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (isOnClick && (Math.abs(xClicked - event.getX() / getWidth()) > SCROLL_THRESHOLD ||
                        Math.abs(yClicked - event.getY() / getHeight()) > SCROLL_THRESHOLD)) {
                    isOnClick = false;
                }
                break;
        }

        return super.onTouchEvent(event);
    }

    /**
     * Moves to the next page.
     */
    private void next() {
        if (currentPage == maxPage)
            activity.moveToNextSpineItem();
        else {
            currentPage++;
            loadUrl("javascript:next()");
        }
    }

    /**
     * Moves to the previous page.
     */
    private void previous() {
        if (currentPage == 1)
            activity.moveToPreviousSpineItem();
        else {
            currentPage--;
            loadUrl("javascript:previous()");
        }
    }

    @JavascriptInterface
    public void onDefineWord(String value) {
        activity.findDefinitions(value);
    }

    @JavascriptInterface
    public void onLoadAndReturnMaxPage(String page) {
        maxPage = Integer.valueOf(page);
        currentPage = maxPage;
    }

    @JavascriptInterface
    public void onGetCurrentAndMaxPage(final String page) {
        String[] values = page.split(":");
        currentPage = Integer.valueOf(values[0]);
        maxPage = Integer.valueOf(values[1]);
    }

    /**
     * Action mode for define word.
     */
    private class DefineWordActionModeCallback implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.reading_context_menu, menu);
            defineActionModeShown = true;
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.define_word:
                    loadUrl("javascript:jsInterface.onDefineWord(getSelectedText())");
                    break;
                default:
                    throw new IllegalStateException("Unknown itemId " + item.getItemId());
            }
            mode.finish();
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            clearFocus();
            defineActionModeShown = false;
            actionMode = null;
        }
    }

    /**
     * Action mode for show contents.
     */
    private class ShowContentsActionModeCallback implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.reading_content_menu, menu);
            showContentActionModeShown = true;
            actionMode = mode;
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.show_content:
                    activity.showContentDialog();
                    popDialogShown = true;
                    break;
                default:
                    throw new IllegalStateException("Unknown itemId " + item.getItemId());
            }
            mode.finish();
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            clearFocus();
            showContentActionModeShown = false;
            actionMode = null;
        }
    }

    /**
     * Changes background to black color.
     * @return True if succeeded, false otherwise.
     */
    public boolean changeToBlackBackground() {
        if (loading)
            return false;
        loadUrl("javascript:changeToBlackBackground()");
        return true;
    }

    /**
     * Changes background to white color.
     * @return True if succeeded, false otherwise.
     */
    public boolean changeToWhiteBackground() {
        if (loading)
            return false;
        loadUrl("javascript:changeToWhiteBackground()");
        return true;
    }

}
