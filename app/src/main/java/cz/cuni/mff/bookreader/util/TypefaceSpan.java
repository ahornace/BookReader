package cz.cuni.mff.bookreader.util;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v4.util.LruCache;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;

public class TypefaceSpan extends MetricAffectingSpan {

    private static int CACHE_SIZE = 12;
    private static LruCache<String, Typeface> typefaceCache = new LruCache<>(CACHE_SIZE);

    private Typeface typeface;

    private TypefaceSpan(Context context, String typefaceName) {
        typeface = typefaceCache.get(typefaceName);

        if (typeface == null) {
            typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + typefaceName);
            typefaceCache.put(typefaceName, typeface);
        }
    }

    @Override
    public void updateMeasureState(TextPaint p) {
        p.setTypeface(typeface);
        p.setFlags(p.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    @Override
    public void updateDrawState(TextPaint tp) {
        tp.setTypeface(typeface);
        tp.setFlags(tp.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    public static SpannableString getTypefacedString(Context context, String title, String font) {
        SpannableString s = new SpannableString(title);
        s.setSpan(new TypefaceSpan(context, font), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return s;
    }
}
