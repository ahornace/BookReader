package cz.cuni.mff.bookreader.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import cz.cuni.mff.bookreader.R;
import cz.cuni.mff.bookreader.adapter.ContentAdapter;
import cz.cuni.mff.bookreader.adapter.DefineWordsListAdapter;
import cz.cuni.mff.bookreader.db.BookDbHelper;
import cz.cuni.mff.bookreader.model.BookModel;
import cz.cuni.mff.bookreader.model.ContentItemModel;
import cz.cuni.mff.bookreader.model.WordDefinitionsDialogModel;
import cz.cuni.mff.bookreader.model.WordDefinition;
import cz.cuni.mff.bookreader.util.DictionaryUtils;
import cz.cuni.mff.bookreader.util.HtmlUtils;
import cz.cuni.mff.bookreader.util.XmlUtils;
import cz.cuni.mff.bookreader.view.EbookWebView;

public class ReadingActivity extends AppCompatActivity implements SensorEventListener {

    private static final String TAG = ReadingActivity.class.getName();

    private BookModel book;
    private Map<String, String> items;
    private EbookWebView webView;

    private DocumentBuilder documentBuilder;

    private int currentSpineItem;

    //this is default font size, that we set the webview to have;
    //the font size, that is set in settings sets fonts size in rem, which depends on this
    private static final int DEFAULT_FONT_SIZE = 12;

    private SensorManager sensorManager;
    private Sensor lightSensor;

    private static final float lightThreshold = 3.4f; //according to the wikipedia represents: Dark limit of civil twilight under a clear sky

    private boolean whiteBackground = true;

    private SharedPreferences prefs;

    private boolean changeBackgroundAccordingToLight;

    private FrameLayout frameLayout;
    private View contentDialog;
    private View defineWordDialog;
    private View viewInFrameLayout;

    private TextView defineWordTitle;
    private final List<WordDefinition> definitions = new ArrayList<>();
    private DefineWordsListAdapter defineWordAdapter;
    private ContentAdapter contentAdapter;

    public static final String BOOK_CONTAINER_NAME = "cz_cuni_mff_bookreader.html";

    /**
     * Activity initialization.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_reading);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        contentDialog = inflater.inflate(R.layout.reading_list_dialog, null);
        defineWordDialog = inflater.inflate(R.layout.reading_list_dialog, null);
        frameLayout = (FrameLayout) findViewById(R.id.frame_layout);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        changeBackgroundAccordingToLight = prefs.getBoolean(getString(R.string.pref_change_bg_according_to_light), true);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if (lightSensor == null) {
            Log.e(TAG, "Light sensor not present, cannot implement function for changing background.");
        }

        webView = (EbookWebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setDefaultFontSize(DEFAULT_FONT_SIZE);

        Bundle b = getIntent().getExtras();
        Long bookId = null;
        if (b != null) {
            bookId = b.getLong(getString(R.string.book_id_passed));
        } else {
            Log.e(TAG, "No book passed as an argument.");
            finish();
        }
        if (bookId == null) {
            Log.e(TAG, "No book passed as an argument.");
            finish();
        }

        book = BookDbHelper.loadBook(bookId, this);
        Document content = getContent(book);

        items = loadItemsMap(content);
        List<ContentItemModel> contentItems = loadContentItems(content);
        initDialogs(contentItems);

        if (book.getSpineItem() != null)
            currentSpineItem = book.getSpineItem();
        else
            currentSpineItem = 0;

        double page = 0;
        if (book.getPage() != null)
            page = book.getPage();

        loadFileToWebView(currentSpineItem, page, false, false, null);
    }

    private Document getContent(BookModel book) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            documentBuilder = dbFactory.newDocumentBuilder();
            return documentBuilder.parse(new File(book.getContentFileLocation()));
        } catch (Exception e) {
            Log.e(TAG, "Error occurred while processing content.opf of book " + book.getTitle() +
                    ". Error msg " + e.getMessage());
            finish();
        }
        return null;
    }

    /**
     * Initializes DefineWordDialog and ShowContentsDialog.
     */
    private void initDialogs(List<ContentItemModel> contentItems) {
        defineWordAdapter = new DefineWordsListAdapter(this, definitions);

        defineWordTitle = (TextView) defineWordDialog.findViewById(R.id.dialogTitle);
        ListView dialogList = (ListView) defineWordDialog.findViewById(R.id.dialogList);
        dialogList.setAdapter(defineWordAdapter);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Calendas_Plus.otf");
        defineWordTitle.setTypeface(typeface);

        TextView contentTitle = (TextView) contentDialog.findViewById(R.id.dialogTitle);
        contentTitle.setText("Content");
        contentTitle.setTypeface(typeface);

        contentItems.get(currentSpineItem).setActive(true);

        contentAdapter = new ContentAdapter(this, contentItems);
        ListView contents = (ListView) contentDialog.findViewById(R.id.dialogList);
        contents.setAdapter(contentAdapter);
        contents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                loadFileToWebView(position, 0, false, false, null);
            }
        });
    }

    //TODO: documentation
    private List<ContentItemModel> loadContentItems(Document content) {
        List<ContentItemModel> contentItems = new ArrayList<>();
        Element spine = XmlUtils.getFirstElementWithName(content.getDocumentElement(), "spine");
        if (spine == null) {
            Log.e(TAG, "Cannot find spine tag in content file.");
            finish();
            return contentItems;
        }
        NodeList spineNodes = spine.getElementsByTagName("itemref");

        for (int i = 0; i < spineNodes.getLength(); i++) {
            Element e = (Element) spineNodes.item(i);
            ContentItemModel model = new ContentItemModel();
            String id = e.getAttribute("idref");
            model.setId(id);
            model.setActive(false);
            contentItems.add(model);
        }

        return contentItems;
    }

    //TODO: documentation
    private Map<String, String> loadItemsMap(Document content) {
        Map<String, String> itemsMap = new HashMap<>();

        Element manifest = XmlUtils.getFirstElementWithName(content.getDocumentElement(), "manifest");
        if (manifest == null) {
            Log.e(TAG, "Cannot find manifest tag in content file.");
            finish();
            return itemsMap;
        }
        NodeList itemNodes = manifest.getElementsByTagName("item");

        for (int i = 0; i < itemNodes.getLength(); i++) {
            Element e = (Element) itemNodes.item(i);
            itemsMap.put(e.getAttribute("id"), e.getAttribute("href"));
        }

        return itemsMap;
    }

    //TODO: documentation

    /**
     * Changes part of the book, typically chapter to the next one.
     */
    public void moveToNextSpineItem() {
        List<ContentItemModel> contentItems = contentAdapter.getContentItems();
        if (currentSpineItem == contentItems.size() - 1) //we are at the end, cannot move further
            return;
        loadFileToWebView(currentSpineItem + 1, 0, false, false, null);
    }

    /**
     * Changes part of the book, typically chapter to the previous one.
     */
    public void moveToPreviousSpineItem() {
        if (currentSpineItem == 0) //we are at the start, cannot move backwards
            return;
        loadFileToWebView(currentSpineItem - 1, 0, true, false, null);
    }

    /**
     * Loads file to the webView with specified url.
     * @param url Url to be loaded.
     */
    public void loadFileToWebView(String url) {
        int hashPosition = url.lastIndexOf('#');
        boolean isTargetedOnTag = false;

        String targetId = null;
        if (hashPosition > 0) {
            isTargetedOnTag = true;
            targetId = url.substring(hashPosition + 1, url.length());
            url = url.substring(0, hashPosition);
        }

        File contentFile = new File(book.getContentFileLocation());
        for (String s : items.values()) { //find correct spine item that it is referencing to
            File f = new File(contentFile.getParentFile(), s);

            String decodedUrl = "";
            try {
                decodedUrl = URLDecoder.decode(url, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, "Unsupported encoding " + e.getMessage());
                Toast.makeText(this, "Could not open given url because of unknown encoding.",
                        Toast.LENGTH_SHORT).show();
                finish();
            }

            File file = new File(decodedUrl);

            if (f.equals(file)) {
                List<ContentItemModel> contentItems = contentAdapter.getContentItems();
                for (int i = 0; i < contentItems.size(); i++) {
                    if (items.get(contentItems.get(i).getId()).equals(s)) {
                        loadFileToWebView(i, 0, false, isTargetedOnTag, targetId);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Loads file to webview.
     * @param spineItem Spine item to load.
     * @param page Page in % that we are supposed to load.
     * @param loadMaxPage Specifies if we should load maximum page of given part(chapter).
     * @param loadPageWithId Specifies whether we should load page that contains specific element with targetId.
     * @param targetId Specifies id of the element where we want to go.
     */
    private void loadFileToWebView(int spineItem, double page, boolean loadMaxPage,
                                   boolean loadPageWithId, String targetId) {
        if (currentSpineItem != spineItem) {
            changeActiveSpineItem(spineItem);
            currentSpineItem = spineItem;
        }
        List<ContentItemModel> contentItems = contentAdapter.getContentItems();
        String path = items.get(contentItems.get(spineItem).getId());
        File contentFile = new File(book.getContentFileLocation());
        File bookFile = new File(contentFile.getParentFile(), path);
        File file = new File(bookFile.getParentFile() + File.separator + BOOK_CONTAINER_NAME); //in directory where the html file is specified because of the relative paths

        try {

            Document doc = documentBuilder.parse(bookFile);
            String head = XmlUtils.getFirstTagToStringWithName("head", doc);
            String content = XmlUtils.getFirstTagToStringWithName("body", doc);

            int fontSizeFromPref = prefs.getInt(getString(R.string.pref_font_size_key), DEFAULT_FONT_SIZE);
            float fontSizeRem = ((float) fontSizeFromPref) / DEFAULT_FONT_SIZE;
            String fontFamily = prefs.getString(getString(R.string.pref_font_key), "Calendas_Plus");

            String bookHtml = HtmlUtils.modifyBookContainer(
                    this, head, content, page, loadMaxPage, loadPageWithId, targetId, fontSizeRem, fontFamily, book.getTitle());

            if (bookHtml == null) {
                Log.e(TAG, "Bookhtml is null, closing activity, reason: could not read from assets");
                Toast.makeText(this, "Error while loading necessary files", Toast.LENGTH_SHORT).show();
                finish();
            }

            if (loadMaxPage) {
                webView.setLoadMaxPage(true);
            } else {
                webView.setLoadMaxPage(false);
            }

            FileWriter writer = new FileWriter(file);
            writer.write(bookHtml);
            writer.flush();
            writer.close();

        } catch (SAXException|IOException e) {
            Log.e(TAG, "Error during creating html book file " + e.getMessage());
            Toast.makeText(this, "Could not open book.", Toast.LENGTH_SHORT).show();
            finish();
        }
        webView.loadUrl(Uri.fromFile(file).toString());
        if (viewInFrameLayout != null)
            popDialog();
    }

    private void changeActiveSpineItem(int newSpineItem) {
        List<ContentItemModel> contentItems = contentAdapter.getContentItems();
        contentItems.get(currentSpineItem).setActive(false);
        contentItems.get(newSpineItem).setActive(true);
        contentAdapter.notifyDataSetChanged();
    }

    /**
     * Activity resume.
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (lightSensor != null && changeBackgroundAccordingToLight)
            sensorManager.registerListener(this, lightSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    /**
     * Activity pause. We save page that user was at.
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (lightSensor != null && changeBackgroundAccordingToLight)
            sensorManager.unregisterListener(this);

        double page = ((double) webView.getCurrentPage() - 1) / (webView.getMaxPage() - 1); //-1 because we want % of the current page
        BookDbHelper.updatePage(book.getId(), currentSpineItem, page, this);
    }

    /**
     * Light sensor informs us about change.
     * @param event Contains values about light that shines on device.
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.values[0] < lightThreshold) { //event.values[0] represents light in lx
            if (whiteBackground) {
                if (webView.changeToBlackBackground())
                    whiteBackground = false;
            }
        } else {
            if (!whiteBackground) {
                if (webView.changeToWhiteBackground())
                    whiteBackground = true;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void showContentDialog() {
        frameLayout.addView(contentDialog);
        viewInFrameLayout = contentDialog;
    }

    private void showDefineWordDialog() {
        frameLayout.addView(defineWordDialog);
        viewInFrameLayout = defineWordDialog;
    }

    public void popDialog() {
        frameLayout.removeView(viewInFrameLayout);
        viewInFrameLayout = null;
    }

    public void findDefinitions(String searchedWord) {
        final WordDefinitionsDialogModel model = DictionaryUtils.getWordDefinition(this, searchedWord);
        if (model.getDefinitions().size() == 0) {
            Toast.makeText(this, "No definition found", Toast.LENGTH_SHORT).show();
        } else {
            runOnUiThread(new Runnable() { //is called from javascript interface, so we need to run in UI thread
                @Override
                public void run() {
                    defineWordTitle.setText(model.getDialogTitle());
                    definitions.clear();
                    definitions.addAll(model.getDefinitions());
                    showDefineWordDialog();
                    webView.setPopDialogShown(true);
                    defineWordAdapter.notifyDataSetChanged();
                }
            });
        }
    }

}
