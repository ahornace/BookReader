package cz.cuni.mff.bookreader.enums;

/**
 * Represents word type.
 * Duplicating enum ordinal functionality for db security, if someone would insert new word type,
 * then it would be necessary to add it to the end of the enum.
 */
public enum WordType {
    ADJECTIVE(0),
    ADVERB(1),
    NOUN(2),
    VERB(3);

    private final int value;

    WordType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static WordType getType(int value) {
        switch (value) {
            case 0:
                return ADJECTIVE;
            case 1:
                return ADVERB;
            case 2:
                return NOUN;
            case 3:
                return VERB;
            default:
                throw new IllegalArgumentException("Unknown value " + value);
        }
    }

    @Override
    public String toString() {
        switch (this) {
            case ADJECTIVE:
                return "adjective";
            case ADVERB:
                return "adverb";
            case NOUN:
                return "noun";
            case VERB:
                return "verb";
            default:
                throw new IllegalStateException("Unknown value " + this.value);
        }
    }
}
