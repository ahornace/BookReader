package cz.cuni.mff.bookreader.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.cuni.mff.bookreader.R;
import cz.cuni.mff.bookreader.adapter.NavListAdapter;
import cz.cuni.mff.bookreader.db.DictionaryDbHelper;
import cz.cuni.mff.bookreader.fragment.BookshelfFragment;
import cz.cuni.mff.bookreader.fragment.RecentBooksFragment;
import cz.cuni.mff.bookreader.fragment.SettingsFragment;
import cz.cuni.mff.bookreader.model.NavItemModel;
import cz.cuni.mff.bookreader.util.TypefaceSpan;

/**
 * Main Activity that contains Drawer Layout and shows following fragments: BookshelfFragment,
 * RecentBooksFragment, SettingsFragment.
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();

    private List<NavItemModel> navItems;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private RelativeLayout navPane;
    private ListView navList;

    private int selectedItem = 0;
    private int actualFragment = 0;

    private SharedPreferences prefs;

    private Fragment libraryFragment;
    private Fragment settingsFragment;

    private static final int RECENT_BOOKS = 0;
    private static final int LIBRARY = 1;
    private static final int SETTINGS = 2;

    private final AdapterView.OnItemClickListener itemClickListener = new OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            navList.setItemChecked(position, true);
            selectedItem = position;
            drawerLayout.closeDrawer(navPane);
        }
    };

    private final DrawerLayout.SimpleDrawerListener drawerListener = new DrawerLayout.SimpleDrawerListener() {

        /**
         * Call changes from onDrawerClosed to prevent the UI thread from lagging.
         * @param drawerView
         */
        @Override
        public void onDrawerClosed(View drawerView) {
            if (actualFragment == selectedItem)
                return;
            navList.setItemChecked(selectedItem, true);
            setToolbarTitle(navItems.get(selectedItem).getTitle(), "Calendas_Plus.otf");
            selectItemFromDrawer(selectedItem);
        }

    };

    /**
     * Initializes activity.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefs = getPreferences(MODE_PRIVATE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
        } else {
            Log.e(TAG, "Cannot get instance of action bar.");
            finish();
        }

        navItems = new ArrayList<>();
        navItems.add(new NavItemModel(getString(R.string.recently_read), R.drawable.recently_read_logo));
        navItems.add(new NavItemModel(getString(R.string.bookshelf), R.drawable.bookshelf_logo));
        navItems.add(new NavItemModel(getString(R.string.settings), R.drawable.settings_logo));

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerLayout.setDrawerListener(drawerListener);
        navPane = (RelativeLayout) findViewById(R.id.navPane);
        navList = (ListView) findViewById(R.id.navList);

        drawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);

        NavListAdapter adapter = new NavListAdapter(this, navItems);
        navList.setAdapter(adapter);

        setToolbarTitle(navItems.get(0).getTitle(), "Calendas_Plus.otf");

        navList.setOnItemClickListener(itemClickListener);

        if (!prefs.getBoolean(getString(R.string.dictionary_initialized), false)) {
            new Thread(new DictionaryInitializationRunnable()).start();
            Log.i(TAG, "Starting to initialize dictionary.");
        }

        navList.setItemChecked(0, true);
        selectItemFromDrawer(0);
    }

    /**
     * Selects item from Drawer Layout on the given position.
     * @param position Position to select from Drawer Layout.
     */
    private void selectItemFromDrawer(int position) {
        FragmentManager fragmentManager = getFragmentManager();
        switch (position) {
            case RECENT_BOOKS:
                Fragment fragment = new RecentBooksFragment();
                fragmentManager.beginTransaction().replace(R.id.mainContent, fragment).commit();
                break;
            case LIBRARY:
                if (libraryFragment == null) {
                    libraryFragment = new BookshelfFragment();
                }
                fragmentManager.beginTransaction().replace(R.id.mainContent, libraryFragment).commit();
                break;
            case SETTINGS:
                if (settingsFragment == null) {
                    settingsFragment = new SettingsFragment();
                }
                fragmentManager.beginTransaction().replace(R.id.mainContent, settingsFragment).commit();
                break;
            default:
                Log.e(TAG, "Illegal position in navigation list " + position);
                return;
        }
        actualFragment = position;
    }

    /**
     * Sets toolbar title with specific typeface.
     * @param title Title to set.
     * @param font Font for the title.
     */
    private void setToolbarTitle(String title, String font) {
        setTitle(TypefaceSpan.getTypefacedString(this, title, font));
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     * Runnable that initializes dictionary i.e. reads sql commands from file and executes them.
     */
    private class DictionaryInitializationRunnable implements Runnable {

        private final String TAG = DictionaryInitializationRunnable.class.getName();

        @Override
        public void run() {
            boolean result = DictionaryDbHelper.initDb(MainActivity.this);
            if (result) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean(getString(R.string.dictionary_initialized), true);
                editor.apply();

                Log.i(TAG, "Dictionary initialization finished successfully.");
            }
        }
    }
}
