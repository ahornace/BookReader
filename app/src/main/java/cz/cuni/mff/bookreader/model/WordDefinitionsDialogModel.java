package cz.cuni.mff.bookreader.model;

import java.util.List;

/**
 * Represents data needed for showing word definitions.
 */
public class WordDefinitionsDialogModel {

    private String dialogTitle; //represents word that the result was found for

    private List<WordDefinition> definitions; //represents word definitions found for dialogTitle

    public String getDialogTitle() {
        return dialogTitle;
    }

    public void setDialogTitle(String dialogTitle) {
        this.dialogTitle = dialogTitle;
    }

    public List<WordDefinition> getDefinitions() {
        return definitions;
    }

    public void setDefinitions(List<WordDefinition> definitions) {
        this.definitions = definitions;
    }
}
