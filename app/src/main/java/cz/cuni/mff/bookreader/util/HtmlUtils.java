package cz.cuni.mff.bookreader.util;

import android.content.Context;
import android.os.Parcelable;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.cuni.mff.bookreader.R;
import cz.cuni.mff.bookreader.db.BookDbHelper;

public class HtmlUtils {

    private static final String TAG = HtmlUtils.class.getName();

    private static final String BOOK_CONTAINER_TEMPLATE = "html/book_container.html";

    private static final Pattern HEAD_REGEX = Pattern.compile("\\<head\\>.*\\</head\\>", Pattern.DOTALL);
    private static final Pattern BODY_REGEX = Pattern.compile("\\<body.*\\</body\\>", Pattern.DOTALL);

    //TODO: add context to documentation
    /**
     * Creates a html webpage for webview to load.
     * @param head String that represents content of head tag of epub html page.
     * @param content String that represents content of body tag of epub html page.
     * @param page Page that we want to load. (in %)
     * @param loadMaxPage Indicates if we want to load max page.
     * @param loadPageWithId Indicates if we want to load page that contains element with specific id.
     * @param targetId Specifies id of the element that we want to load page at.
     * @return Returns html page for webview to load.
     */
    public static String modifyBookContainer(Context context, String head, String content, double page, boolean loadMaxPage,
                                       boolean loadPageWithId, String targetId, float fontSize, String fontFamily, String bookName) {

        String template = loadAssetToString(context, BOOK_CONTAINER_TEMPLATE);
        if (template == null)
            return null;

        if (head == null) {
            head = "";
        } else {
            Matcher m = HEAD_REGEX.matcher(head);
            if (!m.matches())
                head = "";
            else {
                head = head.substring(6);
                head = head.substring(0, head.length() - 7);
            }
        }

        String attr;
        if (content == null) {
            content = "";
            attr = "";
        } else {
            Matcher m = BODY_REGEX.matcher(content);
            if (!m.matches()) {
                content = "";
                attr = "";
            } else {
                boolean escaped = false;
                int j = 0;
                while (true) {
                    char c = content.charAt(j);
                    if (c == '"')
                        escaped = !escaped;
                    if (c == '>' && !escaped)
                        break;
                    j++;
                }
                attr = content.substring(5, j);
                content = content.substring(j + 1);
                content = content.substring(0, content.length() - 7);
            }
        }

        if (loadMaxPage) {
            template = template.replace("${page}", "1");
        } else {
            template = template.replace("${page}", "" + page);
        }

        if (loadPageWithId) {
            template = template.replace("${loadPageWithId}", "true");
            template = template.replace("${targetId}", targetId);
        } else {
            template = template.replace("${loadPageWithId}", "false");
        }

        if (attr.length() > 0)
            template = template.replace("${attr}", attr);
        template = template.replace("${fontSize}", "" + fontSize); //we want to set rem font size = need to divide by the root element font size
        template = template.replace("${font-family}", fontFamily);
        template = template.replace("${bookName}", bookName);
        template = template.replace("${content}", content);
        template = template.replaceFirst(Pattern.quote("${header}"), head);

        return template;
    }

    private static String loadAssetToString(Context context, String assetFile) {
        try {
            InputStream is = context.getAssets().open(assetFile);
            final int size = is.available();
            final byte[] buffer = new byte[size];
            StringBuilder sb = new StringBuilder();

            while (is.read(buffer) != -1) {
                sb.append(new String(buffer));
            }
            is.close();

            return sb.toString();
        } catch (IOException e) {
            Log.e(TAG, "Error occurred while trying to read the asset file " + assetFile);
            return null;
        }
    }

}
