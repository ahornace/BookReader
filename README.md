# BookReader
Simple Android BookReader app.

## User manual

### Main
When you turn on the application you can choose from DrawerLayout 3 different options:

1.  Recently read
2.  Bookshelf
3.  Settings

#### Recently read
Shows books starting from the ones that were most recent opened.

#### Bookshelf
Shows all books ordered by their name.

#### Settings
Shows the following settings for the application:

1.  Change background to black when in dim light – changes background to black and font to white in dim light.
2.  Font – changes font of the book.
3.  Font size – changes font size of the book.

### Reading
Shows the book for reading.

#### Navigation
You can navigate through the book by tapping on right or left side of the display or by
sweeping to the right or to the left.

#### Using dictionary
If you press long on some word, then you can click on "Define word" option that
shows you synonyms from Open Office Thesaurus database.

#### Showing content
If you tap in the middle of the screen, then you can click on "Show contents" option that
shows you the contents list and the current item that you are on is shown in bold.

### Minimum API Version
* 15 (Android ICS 4.0.3)

## TODO
* Implement MOBI support.
* Implement content chapters names by the headings in the html files.
* Improve parsing of epubs.
* Count pages for whole book, not just chapters.
* Change font for ActionMode and ActionBar menus.
* Change color of ActionMode arrows to white.

## Known issues
* Missing closing tags for \<a\> and \<script\> tags can produce undefined results while reading(most of the time only 
current page isn't showing)
* On Nexus 7 (2013) – API 23 – sometimes webview doesn't show css columns properly.

## Tested on devices
* Nexus 7 (2013)
* LG Optimus 4x HD (P880)
